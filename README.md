# QBCollectionInfo



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/shuaishaoqiubite/qbcollectioninfo.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/shuaishaoqiubite/qbcollectioninfo/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

## WxValidate - 表单验证

## 插件介绍

该插件是参考 jQuery Validate 封装的，为小程序表单提供了一套常用的验证规则，包括手机号码、电子邮件验证等等，同时提供了添加自定义校验方法，让表单验证变得更简单。

## 参数说明

| 参数     | 类型                | 描述               |
| -------- | ------------------- | ------------------ |
| rules    | <code>object</code> | 验证字段的规则     |
| messages | <code>object</code> | 验证字段的提示信息 |

## 内置校验规则

| 序号 | 规则                              | 描述                                                    |
| ---- | --------------------------------- | ------------------------------------------------------- |
| 1    | <code>required: true</code>       | 这是必填字段。                                          |
| 2    | <code>email: true</code>          | 请输入有效的电子邮件地址。                              |
| 3    | <code>tel: true</code>            | 请输入11位的手机号码。                                  |
| 4    | <code>url: true</code>            | 请输入有效的网址。                                      |
| 5    | <code>date: true</code>           | 请输入有效的日期。                                      |
| 6    | <code>dateISO: true</code>        | 请输入有效的日期（ISO），例如：2009-06-23，1998/01/22。 |
| 7    | <code>number: true</code>         | 请输入有效的数字。                                      |
| 8    | <code>digits: true</code>         | 只能输入数字。                                          |
| 9    | <code>idcard: true</code>         | 请输入18位的有效身份证。                                |
| 10   | <code>equalTo: 'field'</code>     | 输入值必须和 field 相同。                               |
| 11   | <code>contains: 'ABC'</code>      | 输入值必须包含 ABC。                                    |
| 12   | <code>minlength: 5</code>         | 最少要输入 5 个字符。                                   |
| 13   | <code>maxlength: 10</code>        | 最多可以输入 10 个字符。                                |
| 14   | <code>rangelength: [5, 10]</code> | 请输入长度在 5 到 10 之间的字符。                       |
| 15   | <code>min: 5</code>               | 请输入不小于 5 的数值。                                 |
| 16   | <code>max: 10</code>              | 请输入不大于 10 的数值。                                |
| 17   | <code>range: [5, 10]</code>       | 请输入范围在 5 到 10 之间的数值。                       |

## 常用实例方法

| 名称                             | 返回类型             | 描述                                   |
| -------------------------------- | -------------------- | -------------------------------------- |
| checkForm(e)                     | <code>boolean</code> | 验证所有字段的规则，返回验证是否通过。 |
| valid()                          | <code>boolean</code> | 返回验证是否通过。                     |
| size()                           | <code>number</code>  | 返回错误信息的个数。                   |
| validationErrors()               | <code>array</code>   | 返回所有错误信息。                     |
| addMethod(name, method, message) | <code>boolean</code> | 添加自定义验证方法。                   |

## addMethod(name, method, message) - 添加自定义校验

第一个参数 name 是添加的方法的名字。
第二个参数 method 是一个函数，接收三个参数 (value, param) ，value 是元素的值，param 是参数。
第三个参数 message 是自定义的错误提示。

## 使用说明

```js
// 验证字段的规则
const rules = {
    email: {
        required: true,
        email: true,
    },
    tel: {
        required: true,
        tel: true,
    },
    idcard: {
        required: true,
        idcard: true,
    },
}

// 验证字段的提示信息，若不传则调用默认的信息
const messages = {
    email: {
        required: '请输入邮箱',
        email: '请输入正确的邮箱',
    },
    tel: {
        required: '请输入手机号',
        tel: '请输入正确的手机号',
    },
    idcard: {
        required: '请输入身份证号码',
        idcard: '请输入正确的身份证号码',
    },
}

// 创建实例对象
this.WxValidate = new WxValidate(rules, messages)

// 自定义验证规则
this.WxValidate.addMethod('assistance', (value, param) => {
    return this.WxValidate.optional(value) || (value.length >= 1 && value.length <= 2)
}, '请勾选1-2个敲码助手')

// 如果有个表单字段的 assistance，则在 rules 中写
assistance: {
    required: true,
    assistance: true,
},

// 调用验证方法，传入参数 e 是 form 表单组件中的数据
submitForm(e) {
    const params = e.detail.value

    console.log(params)

    // 传入表单数据，调用验证方法
    if (!this.WxValidate.checkForm(e)) {
        const error = this.WxValidate.errorList[0]
        return false
    }
},
```