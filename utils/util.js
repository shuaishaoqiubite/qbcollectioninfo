const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
const app = getApp();
import {
  apiUrl
} from "./config.js"

module.exports = {
  formatTime: formatTime,
  navToByObj: navToByObj,
  navTo: navTo,
  post: post
}

//打开新页面传递json对象参数
function navToByObj(path, jsonObj) {
  if (jsonObj) {
    var json = JSON.stringify(jsonObj)
    wx.navigateTo({
      url: path + '?value=' + json
    })

  } else {
    wx.navigateTo({
      url: path
    })
  }
}
//打开新页面传递字符串参数
function navTo(path, str) {
  if (str) {
    wx.navigateTo({
      url: path + '?value=' + str
    })

  } else {
    wx.navigateTo({
      url: path
    })
  }
}


/**
 * http post请求
 */
function post(url, method, jsonMap, succCallback, failCallback, showLoading, showToast) {
  if (showLoading == undefined || showLoading == true) {
    wx.showLoading({
      title: '加载中..',
      mask: true
    })
  }
  var path = apiUrl + url
  console.log('path:', path)
  var map = {}

  for (var key in jsonMap) {
    map[key] = jsonMap[key];
  }

  let hearder = {
    'content-type': 'application/json' // 默认值
  }
  if (wx.getStorageSync('token')) {
    // map.token =  wx.getStorageSync('token')
    hearder = {
      'content-type': 'application/json', // 默认值
      'token': wx.getStorageSync('token')

    }
  }


  wx.request({
    url: path,
    method: method,
    data: map,
    header: hearder,
    success(res) {
      wx.hideLoading()
      wx.stopPullDownRefresh()

      if (res.statusCode == 200) {
        const requestData = res.data
        if (requestData.code == 0) {
          succCallback(requestData)
        } else if (res.data.code == 401) {
          wx.showToast({
            title: '登录过期，请重新登录',
            icon: 'none',
            duration: 1000
          })
          setTimeout(() => {
            wx.navigateTo({
              url: '/pages/login/login',
            })
          }, 1000);

        } else {
          if (res.data.msg && showToast == undefined) {
            wx.showToast({
              title: res.data.msg,
              icon: 'none',
              duration: 2000
            })
          }
          failCallback(res.data)
        }

      } else {
        wx.showToast({
          title: 'request error',
          icon: 'none',
        })
      }
    },
    //网络异常回调
    fail(error) {
      wx.hideLoading();
      wx.stopPullDownRefresh()

      wx.showToast({
        title: '网络异常，请检查',
        icon: 'none',
      })

    }
  })
}