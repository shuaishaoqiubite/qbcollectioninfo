const util = require('../../../utils/util.js');
import WxValidate from '../../../utils/WxValidate.js'

Page({
  data: {
    appeal: "",
    cardNumber: "",
    homeAddress: "",
    personName: "",
    phone: "",
    workAddress: "",
    workDept: "",
    cardType: '',
    sex: '0',
    cardType: '0',
    sexArray: ['男', '女'],
    cardTypeArray: ['居民身份证', '护照', '其他'],
    dataId: null
  },
  formSubmit(e) {
    console.log(e)
    const params = e.detail.value

    const sexname = params.sex
    params.sex = this.data.sexArray[sexname]

    const cardTypename = params.cardType
    params.cardType = this.data.cardTypeArray[cardTypename]

    if (!this.WxValidate.checkForm(params)) {
      const error = this.WxValidate.errorList[0]
      this.showModal(error)
      return false
    }

    var url = '/lwcjPerson/save'
    if (this.data.dataId) {
      url = '/lwcjPerson/update'
      params.id = this.data.dataId
    }
    // console.log('url', url)
    util.post(url, 'POST', params,
      function success(data) {
        let pages = getCurrentPages()
        let prevPage = pages[pages.length - 2]
        prevPage.refreshData()

        wx.navigateBack({
          delta: 1,
        })
      },
      function fail(data) {
        console.log(data);
      })

  },
  onLoad(options) {
    console.log(options)
    if (options && options.id) {
      this.setData({
        dataId: options.id
      })
      this.getDataById()
    }
    this.initValidate()
  },
  showModal(error) {
    wx.showModal({
      content: error.msg,
      showCancel: false
    })
  },
  initValidate() {
    const rules = {
      personName: {
        required: true,
      },
      phone: {
        required: true,
      },
      cardType: {
        required: true,
      },
      cardNumber: {
        required: true,
      },
      appeal: {
        required: true,
      },
    }

    const messages = {
      personName: {
        required: '请填写姓名',
        email: '请填写正确的姓名',
      },
      phone: {
        required: '请填写联系电话',
        tel: '请填写正确的联系电话',
      },
      cardType: {
        required: '请选择证件类型',
      },
      cardNumber: {
        required: '请填写证件号码',
        tel: '请填写正确的证件号码',
      },
      appeal: {
        required: '请选择证件类型',
      },
    }
    this.WxValidate = new WxValidate(rules, messages)
  },
  getDataById() {
    const that = this
    util.post('/lwcjPerson/info/' + this.data.dataId, 'GET', {},
      function success(data) {
        console.log('data:', data)
        if (data.code == 0) {
          const requestData = data.user
          let sexIndex = data.user.sex
          that.data.sexArray.forEach((element, index) => {
            if (element == data.user.sex) {
              sexIndex = index
            }
          });

          let cardTypeIndex = data.user.cardType
          that.data.cardTypeArray.forEach((element, index) => {
            if (element == data.user.cardType) {
              cardTypeIndex = index
            }
          });
          console.log('cardTypeiNDEX', cardTypeIndex)

          that.setData({
            appeal: data.user.appeal,
            cardNumber: data.user.cardNumber,
            homeAddress: data.user.homeAddress,
            personName: data.user.personName,
            phone: data.user.phone,
            workAddress: data.user.workAddress,
            workDept: data.user.workDept,
            cardType: data.user.cardType,
            sex: sexIndex,
            cardType: cardTypeIndex
          })
        }
      },
      function fail(data) {
        console.log(data);
      })
  },
  bindSexChange(e) {
    console.log('sex发送选择改变，携带值为', e.detail.value)
    this.setData({
      sex: e.detail.value
    })
  },
  bindcardTypeChange(e) {
    console.log('cardType发送选择改变，携带值为', e.detail.value)
    this.setData({
      cardType: e.detail.value
    })
  }
})