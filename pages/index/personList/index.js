// index.js
// 获取应用实例
const app = getApp()
const util = require('../../../utils/util.js');

Page({
  data: {
    page: 1,
    pageSize: 20,
    dataList: [],
    maxPage: 0
  },

  onShow() {
    if (this.data.dataList.length == 0) {
      this.requestData()
    }
  },
  onLoad() {
    this.requestData()
  },
  onPullDownRefresh: function () {
    this.refreshData()
    wx.stopPullDownRefresh();
  },
  refreshData() {
    var that = this;
    this.setData({
      page: 1
    })
    // 调数据方法
    this.requestData()
  },
  onReachBottom: function () {
    if (this.data.page < this.data.maxPage) {
      const page = this.data.page + 1
      this.setData({
        page: page
      })
      console.log(this.data.page)
      this.requestData()
    }
  },
  requestData() {
    const that = this
    const params = {
      page: this.data.page,
      limit: this.data.pageSize
    }
    util.post('/lwcjPerson/list', 'GET', params,
      function success(data) {
        const requestData = data.page
        if (data.code === 0) {
          const list = requestData.list
          list.forEach(item => {
            item.open = 'false'
          });

          if (that.data.page == 1) {
            that.setData({
              dataList: requestData.list
            })
          } else {
            const dataList = that.data.dataList.concat(list)
            that.setData({
              dataList: dataList
            })
          }

          const maxPage = (requestData.totalCount / that.data.pageSize) + 1
          that.setData({
            maxPage: maxPage
          })
        }
      },
      function fail(data) {
        console.log(data);
      })
  },

  selectButton(e) {
    console.log(e.currentTarget.dataset.index)
  },
  editAction(e) {
    console.log(e.currentTarget.dataset.index)
    const dataId = this.data.dataList[e.currentTarget.dataset.index].id
    wx.navigateTo({
      url: '/pages/index/addPerson/index?id=' + dataId,
    })
  },
  addPerson() {
    wx.navigateTo({
      url: '/pages/index/addPerson/index',
    })
  },
  openAction(e) {
    const index = e.currentTarget.dataset.index
    const dataList = JSON.parse(JSON.stringify(this.data.dataList))
    if (dataList[index].open == "false") {
      dataList[index].open = "true"
    } else {
      dataList[index].open = "false"
    }
    this.setData({
      dataList: dataList
    })
  }


})