const util = require('../../../utils/util.js');
import WxValidate from '../../../utils/WxValidate.js'

Page({
  data: {
    articleHolder: "",
    holderCardNumber: "",
    articleName: "",
    articleDescribe: "",
    articlePrice: "",
    articleType: '0',
    articleTypeArray: ['电子类', '账号类', '不动产类', '其他']
  },
  formSubmit(e) {
    console.log(e)
    const params = e.detail.value

    const articleTypeName = params.articleType
    params.articleType = this.data.articleTypeArray[articleTypeName]

    if (!this.WxValidate.checkForm(params)) {
      const error = this.WxValidate.errorList[0]
      this.showModal(error)
      return false
    }

    var url = '/lwcjArticle/save'
    if (this.data.dataId) {
      url = '/lwcjArticle/update'
      params.id = this.data.dataId
    }
    // console.log('url', url)
    util.post(url, 'POST', params,
      function success(data) {
        let pages = getCurrentPages()
        let prevPage = pages[pages.length - 2]
        prevPage.refreshData()

        wx.navigateBack({
          delta: 1,
        })
      },
      function fail(data) {
        console.log(data);
      })
  },
  onLoad(options) {
    console.log(options)
    if (options && options.id) {
      this.setData({
        dataId: options.id
      })
      this.getDataById()
    }
    this.initValidate()
  },
  showModal(error) {
    wx.showModal({
      content: error.msg,
      showCancel: false
    })
  },
  initValidate() {
    const rules = {
      articleHolder: {
        required: true,
      },
      holderCardNumber: {
        required: true,
      },
      articleTypeArray: {
        required: true,
      },
      articleName: {
        required: true,
      },
    }

    const messages = {
      articleHolder: {
        required: '请填写物品持有人',
        email: '请填写正确的物品持有人',
      },
      holderCardNumber: {
        required: '请填写持有人证件号码',
        tel: '请填写正确的持有人证件号码',
      },
      articleTypeArray: {
        required: '请选择物品类型',
      },
      articleName: {
        required: '请填写物品名称',
        email: '请填写正确的物品名称',
      },
    }
    this.WxValidate = new WxValidate(rules, messages)
  },
  getDataById() {
    const that = this
    util.post('/lwcjArticle/info/' + this.data.dataId, 'GET', {},
      function success(data) {
        console.log('data:', data)
        if (data.code == 0) {
          const requestData = data.user

          let articleTypeIndex = data.user.articleType
          that.data.articleTypeArray.forEach((element, index) => {
            if (element == data.user.articleType) {
              articleTypeIndex = index
            }
          });

          that.setData({
            articleHolder: data.user.articleHolder,
            holderCardNumber: data.user.holderCardNumber,
            articleName: data.user.articleName,
            articleDescribe: data.user.articleDescribe,
            articlePrice: data.user.articlePrice,
            articleType: articleTypeIndex
          })
        }
      },
      function fail(data) {
        console.log(data);
      })
  },
  bindArticleTypeChange(e) {
    console.log('wplx发送选择改变，携带值为', e.detail.value)
    this.setData({
      articleType: e.detail.value
    })
  }
})