// index.js
// 获取应用实例
const app = getApp()
 Page({
  data: {
    functionList: [{ title: '人员采集',image: '/images/public/1.png' }, { title: '物品信息采集' ,image: '/images/public/2.png'}, { title: '车辆信息采集',image: '/images/public/3.png' }]
  },

  onShow() {
    console.log('ssss')
    console.log(11111)
  },
  onLoad() {
    
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  selectButton:function(e) {
    // wx.setStorageSync('token', null)
    if(!wx.getStorageSync('token')){
      wx.navigateTo({
        url: '/pages/login/login',
      })
      return
    }
    console.log(e.target.dataset.index)
    const index = e.target.dataset.index
    if(index == 0 ) {
      wx.navigateTo({
        url: '/pages/index/personList/index',
      })
    }else if(index == 1) {
      wx.navigateTo({
        url: '/pages/index/thingList/index',
      })
    }else if(index == 2) {
      wx.navigateTo({
        url: '/pages/index/carList/index',
      })
    }
  
  }


})
