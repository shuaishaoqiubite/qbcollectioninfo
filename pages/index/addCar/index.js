const util = require('../../../utils/util.js');
import WxValidate from '../../../utils/WxValidate.js'

Page({
  data: {
    carColor: "",
    carHolder: "",
    carNumber: "",
    carType: "",
    factory: "",
    holderCardNumber: "",
    user: ""
  },
  formSubmit(e) {
    console.log(e)
    const params = e.detail.value

    if (!this.WxValidate.checkForm(params)) {
      const error = this.WxValidate.errorList[0]
      this.showModal(error)
      return false
    }

    var url = '/lwcjCar/save'
    if (this.data.dataId) {
      url = '/lwcjCar/update'
      params.id = this.data.dataId
    }
    // console.log('url', url)
    util.post(url, 'POST', params,
      function success(data) {
        let pages = getCurrentPages()
        let prevPage = pages[pages.length - 2]
        prevPage.refreshData()

        wx.navigateBack({
          delta: 1,
        })
      },
      function fail(data) {
        console.log(data);
      })

  },
  onLoad(options) {
    console.log(options)
    if (options && options.id) {
      this.setData({
        dataId: options.id
      })
      this.getDataById()
    }
    this.initValidate()
  },
  showModal(error) {
    wx.showModal({
      content: error.msg,
      showCancel: false
    })
  },
  initValidate() {
    const rules = {
      carHolder: {
        required: true,
      },
      holderCardNumber: {
        required: true,
      },
      carColor: {
        required: true,
      },
    }

    const messages = {
      carHolder: {
        required: '请填写车辆持有人',
        email: '请填写正确的车辆持有人',
      },
      holderCardNumber: {
        required: '请填写持有人证件号码',
        tel: '请填写正确的持有人证件号码',
      },
      carColor: {
        required: '请填写车辆颜色',
        idcard: '请填写正确的车辆颜色',
      },
    }
    this.WxValidate = new WxValidate(rules, messages)
  },
  getDataById() {
    const that = this
    util.post('/lwcjCar/info/' + this.data.dataId, 'GET', {},
      function success(data) {
        console.log('data:', data)
        if (data.code == 0) {
          that.setData({
            carColor: data.user.carColor,
            carHolder: data.user.carHolder,
            carNumber: data.user.carNumber,
            carType: data.user.carType,
            factory: data.user.factory,
            holderCardNumber: data.user.holderCardNumber,
            user: data.user.user
          })
        }
      },
      function fail(data) {
        console.log(data);
      })
  }
})