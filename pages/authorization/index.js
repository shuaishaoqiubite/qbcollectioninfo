// index.js
// 获取应用实例
const app = getApp()
const util = require('../..//utils/util.js');
Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
onShow() {
  console.log('ssss')
  console.log(11111)
},
  onLoad() {
     
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  rebackAction: function() {
    wx.navigateBack()
  },
  lwogin: function(e) {
    if (!e.detail.userInfo) {
      return;
    }
    this.setData({
      wxUser: e.detail.userInfo
    })

    // 回到原来的地方放
    wx.setStorageSync('userInfo', e.detail.userInfo);
  },
  bindGetUserInfo:function(e) {
     wx.redirectTo({
       url: '/pages/login/login',
     })
  },

  unloginbackAciton: function() {
      
    wx.navigateBack()
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getPhoneNumber(e) {
    let that = this
    let d = e.detail
    console.log(e)

    if (d.encryptedData && d.iv) {
      let session_key = wx.getStorageSync("session_key")
      that.parseMobile(that.data.wxCode, d.encryptedData, d.iv, '')

    } else {
      wx.showToast({
        title: '微信登录已取消',
        icon: 'none'
      })
    }
  },
  parseMobile(code, encryptedData, iv, key) {
    let that = this
    let map = {
      js_code: code,
      base64: encryptedData,
      iv: iv,
      session_key : key
    }
    util.post('user/wx_paser_mobile_and_return', map,
      function success(res) {
        // console.log(res.data)
        let rd = res.data
        that.data.wxPhone = res.data.phoneNumber
        that.doLogin()
      },
      function fail(data) {
        console.log(data);
      }
    )
  },

  doLogin(account_id) {
    const that = this
    var map = {
      mobile: this.data.wxPhone,
      code: app.globalData.login_code
       }
    
    util.post('user/login', map,
      function success(res) { 
        wx.reLaunch({
          url: '/pages/home/index',
        });
      },
      function fail(data) {
        console.log(data);
      }
    )
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
