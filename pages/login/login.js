
const util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: '',
    password: '',
    openPassword: false
  },

  usernameInput(e) {
    this.setData({
      userName: e.detail.value
    })
  },

  bindMobile: function (e) {
    if (this.data.userName == '') {
      wx.showToast({
        title: '账号不能为空',
        icon: 'none'
      })
      return
    }

    if (this.data.password == '') {
      wx.showToast({
        title: '密码不能为空',
        icon: 'none'
      })
      return
    }

    const that = this
    const params = {
      userName: this.data.userName,
      password: this.data.password
    }
    util.post('/sys/login', 'GET', params,
      function success(data) {

        if (data.code === 0) {
          wx.setStorageSync('token', data.token)
          wx.navigateBack({
            delta: 1,
          })
        }



      },
      function fail(data) {
        console.log(data);
      })


  },
  clearMobile() {

    this.setData({
      mobile: ''
    })
  },
  openPassword(e) {
    console.log('aaaa')
    const openPassword = this.data.openPassword
    if (openPassword) {
      this.setData({
        openPassword: false
      })
    } else {
      this.setData({
        openPassword: true
      })
    }

  },
  inputAction(e) {

    this.setData({
      password: e.detail.value
    })
  }



})