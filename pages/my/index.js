// index.js
// 获取应用实例
const app = getApp()
const util = require('../..//utils/util.js');
Page({
  data: {
     userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },
   
onShow() {
  console.log('ssss')
  console.log(11111)
},
  onLoad() {
     
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  loginOut() {
    wx.showModal({

			content: "确定要退出当前登录吗？",

			confirmText: "确定",

			cancelText: "取消",

			success: function (res) {

				if (res.confirm) {
          console.log('退出登录')
           
          wx.navigateTo({
            url: '/pages/authorization/index',
            success: function (res) {
              
            }
          })
				}

			},complete: function (res) {

      }
    })
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
